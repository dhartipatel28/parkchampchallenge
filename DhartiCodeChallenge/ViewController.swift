//
//  ViewController.swift
//  DhartiCodeChallenge
//
//  Created by Dharti on 05/12/20.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {
    
    // MARK:- Variable Declaration
    var arrData = [String]()
    var ref : DatabaseReference!
    
    @IBOutlet weak var txtData : UITextField!
    @IBOutlet weak var lblCount : UILabel!
    @IBOutlet weak var btnSearch : UIButton!
    
    // MARK:- Controller Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        setUI ()
        importData()
        fetchData()
    }
    
    // MARK:- importData
    func importData() -> Void {
        let arrtmp = ["acme", "came", "acre", "care", "race", "ales", "leas", "seal"]
        ref.child("test").setValue(arrtmp)
    }
    
    // MARK:- FetchData
    func fetchData(){
        self.ref.child("test").observeSingleEvent(of: .value) { (response) in
            self.arrData = response.value as! [String]
        }
    }
    
    // MARK:- UISet
    func setUI (){
        btnSearch.layer.cornerRadius = 5.0
        btnSearch.layer.borderWidth = 1.0
        btnSearch.layer.borderColor = UIColor.black.cgColor
    }
    
    // MARK:- UIButton IBAction Method
    @IBAction func btnSearchTapped(_ sender : UIButton)
    {
        if txtData.text == "" {
            lblCount.text = "Please enter text"
            lblCount.textColor = .red
        }else{
            lblCount.textColor = .black
            lblCount.text = ""
            self.DataFilter()
        }
    }
    
    // MARK:- Data Search & Filter Method
    func DataFilter (){
        
        guard let searchstring = txtData.text else {
            return
        }
        
        let lblSort : String = String(searchstring.sorted())
        var contAn = 0
        for str in arrData {
            let sortedData = String(str.sorted())
            if sortedData == lblSort {
                contAn += 1
            }
        }
        lblCount.text = "\(contAn) anograms found"
    }
}

